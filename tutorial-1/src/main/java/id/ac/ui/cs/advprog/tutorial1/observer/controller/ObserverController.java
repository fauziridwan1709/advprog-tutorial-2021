package id.ac.ui.cs.advprog.tutorial1.observer.controller;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Quest;
import id.ac.ui.cs.advprog.tutorial1.observer.service.GuildServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ObserverController {

    @Autowired
    private GuildServiceImpl guildService;

    @GetMapping(value = "/create-quest")
    public String createQuest(Model model){
        model.addAttribute("quest", new Quest());
        return "observer/questForm";
    }

    @PostMapping(value = "/add-quest")
    public String addQuest(@ModelAttribute("quest") Quest quest) {
        guildService.addQuest(quest);
        return "redirect:/adventurer-list";
    }

    @GetMapping(value = "/adventurer-list")
    public String getAdventurers(Model model){
        model.addAttribute("adventurers", guildService.getAdventurers());
        return "observer/adventurerList";
    }
}
