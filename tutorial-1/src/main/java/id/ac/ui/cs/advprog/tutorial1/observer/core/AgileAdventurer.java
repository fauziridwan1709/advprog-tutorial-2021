package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        switch(guild.getQuestType()) {
            case "D":
            this.getQuests().add(guild.getQuest());
            break;
            case "R":
            this.getQuests().add(guild.getQuest());
            break;
            default:
            //do nothing
            break;
        }
    }
}
