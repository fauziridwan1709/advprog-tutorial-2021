package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        switch(guild.getQuestType()) {
            case "D":
            this.getQuests().add(guild.getQuest());
            break;
            case "E":
            this.getQuests().add(guild.getQuest());
            break;
            default:
            //do nothing
            break;
        }

    }
}
