package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    @Override
    public String getType() {
        //kreatifitas
        return "Gun";
    }

    @Override
    public String attack() {
        return "Tembak!!!";
    }
}
